//はてなブックマークではてブ数を取得
function countHatebu(url, selector) {
    $.ajax({
      url:'https://b.hatena.ne.jp/entry.count?callback=?',
      dataType:'jsonp',
      data:{
        url:url
      }
    }).done(function(res){
      $(selector).text( res || 0 );
    }).fail(function(){
      $(selector).text('0');
    });
  }
  $(function(){
    countHatebu('{Permalink}', '.hatebu-count');
  });



