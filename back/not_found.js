$(document).ready(function() {
  if ($('#main-inner').children().hasClass("no-entry")) {
    $('#main-inner').html("<div class=\"notfound\">\
    <br/>ページが見つからず、大変申し訳ございません。<br/>\
    最近、大幅にURLの変更を致しました。また、記事の内容も最新のものにアップデートしています。<br/>\
    お手数ですが、下記のトップページから記事を探して頂き、ご覧下さい。<br/>\
    <br/><br/>\
    <ul class=\"table-of-sitemap\">\
      <li>PythonやRaspberry Piの使い方を紹介したブログはこちら ▶ <u><b><a href=\"https://www.raspberrypirulo.net/home\">Python & Raspbery Pi開発ブログ☆彡</a></b></u></li>\
      <li>はてなブログの使い方、カスタマイズについて解説したブログはこちら ▶ <u><b><a href=\"https://www.raspberrypirulo.net/hatena\">はてなブログを使いこなす！</a></b></u></li>\
    </ul>\
    </div>");
    $('.notfound').css('margin','auto 20px');
  }
});