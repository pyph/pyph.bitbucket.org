(function($) {
'use strict';
var urls = [], opts = {cache: false, dataType: 'xml'}, p,
url = 'https://www.raspberrypirulo.net/sitemap.xml';

<!-- 指定されたサイトマップ.xmlからURLを抜き出す関数 -->
function parseSitemapXML(url) {
    var d = new $.Deferred;
    $.ajax($.extend(opts, {
    url: url
    })).done(function(xml) {
    $(xml).find('sitemap').each(function() {
        urls.push($(this).find('loc').text());
    });
    d.resolve();
    }).fail(function() {
    d.reject();
    });
    console.log(urls[0]);
    return d.promise();
}
    
<!-- サイトマップ内のURLと現在表示されているページのURLが一致するまで検索する -->
<!-- 一致した更新日をappendLastmod関数の引数に渡す -->
function findURL(url) {
    console.log(urls[0]);
    $.ajax($.extend(opts, {
    url: url
    })).done(function(xml) {
    var isMatched = false;
    $(xml).find('url').each(function() {
        var $this = $(this);
        if ($this.find('loc').text() === location.href) {
        isMatched = true;
        appendLastmod($this.find('lastmod').text());
        return false;
        }
    });
    if (!isMatched) nextURL();
    }).fail(function() {});
}
    
<!-- 次のURLに移動する -->
function nextURL() {
    urls.shift();
    if (urls.length) findURL(urls[0]);
}
    
<!-- 取得した更新日の不要な部分を置き換え、既存のhtmlに追加して表示する -->
<!-- 横並びにするために、<span></span>を使用 -->
function appendLastmod(lastmod) {
    var $container = $('<span></span>', {'class': 'lastmod'}).text(lastmod.replace(/T.*0/,""));
    if ($('.entry-header > .date').get(0).tagName.toLowerCase() === 'span') {
    $('.entry-title').before($container);
    } else {
    $('.entry-date').append($container);
    }
}

<!-- 上記の関数を実行 -->
p = parseSitemapXML(url);
p.done(function() {findURL(urls[0])});
p.fail(function(error) {});

})(jQuery);
