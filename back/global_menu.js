$(function() {

    var str = location.href.split('/')
    console.log(str[str.length-1].substring(0, 6));
    if (((str[str.length-1].substring(0, 6)) != 'hatena') && (str[str.length-1].substring(0, 6)) != '%E3%81'){
            
        $('#title').html('<a href="https://www.raspberrypirulo.net/home"><font color="#fff">Python &amp; Raspberry Pi 開発ブログ ☆彡</font></a>');
        $('#blog-description').html('<font color="#fff">PythonとRaspberry Piの基本的な使い方をわかりやすく解説。初心者、入門者必見！！</font>');

        $('#sw-menu').before(
        '<div id="menu">\
        <div id="menu-inner">\
            <div id="btn-content">\
                <span id="menu-btn"><i class="blogicon-reorder"></i> MENU</span>\
            </div>\
            <ul id="menu-content">\
                <li class="first-content">\
                    <a href="https://www.raspberrypirulo.net/home"><i class="blogicon-home lg"></i></a>\
                </li>\
                <li class="first-content">\
                    <a href="javascript:void(0)"><img class="rasp-icon" src="https://cdn-ak.f.st-hatena.com/images/fotolife/r/raspberrypi/20191122/20191122221737.png" alt="raspberry pi"/>&nbsp;&nbsp;ラズパイ 設定 ▼</a>\
                    <ul class="second-content">\
                        <li><a href="javascript:void(0)">開発環境　　　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/pipenv">仮想環境(pipenv)</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/vscode">VS Code</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/eclipse-remotesystemexplorer">eclipse</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/teraterm-ssh-macro">Tera Termマクロ SSH自動ログイン</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">ネットワーク　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/wirelesslan">無線LAN設定 DHCP/固定IP</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/ssh">SSH パスワード/鍵認証</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">自動起動　　　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/systemd">Systemd</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/cron">Cron</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">リモート操作　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/remote-desktop">リモートデスクトップ</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/vnc">VNC</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/anydesk">AnyDesk</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">USBデバイス名固定　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/usbname-fix-id">ID指定</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/usbname-fix-port">ポート番号指定</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/samba">Samba設定</a>\
                        </li>\
                    </ul>\
                </li>\
                <li class="first-content">\
                    <a href="javascript:void(0)"><img class="rasp-icon" src="https://cdn-ak.f.st-hatena.com/images/fotolife/r/raspberrypi/20191122/20191122221737.png" alt="raspberry pi"/>&nbsp;&nbsp;ラズパイ ハードウェア ▼</a>\
                    <ul class="second-content">\
                        <li><a href="javascript:void(0)">IO　 　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/gpio">GPIO(Lチカ)</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/python-callback">割り込み(callback関数)</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/python-waitforedge">割り込み(wait for edge)</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">UART　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/uart-setting">シリアル通信設定</a></li>\
                            <li><a href="javascript:void(0)">Pythonプログラム</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">I2C　 　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/raspberrypi-i2c">シリアル通信</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/real-time-clock">リアルタイムクロック</a></li>\
                            </ul>\
                        </li>\
                <li><a href="javascript:void(0)">USB　　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/usb-serial">シリアル通信</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/usbmemory-mount">USBメモリマウント</a></li>\
                            </ul>\
                        </li>\
                <li><a href="javascript:void(0)">LAN　　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/socket-server">Socket通信(サーバー編)</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/socket-client">Socket通信(クライアント編)</a></li>\
                            </ul>\
                        </li>\
                <li><a href="javascript:void(0)">Audio　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="javascript:void(0)">アナログ出力</a></li>\
                            <li><a href="javascript:void(0)">USBスピーカー</a></li>\
                            </ul>\
                        </li>\
                <li><a href="javascript:void(0)">Bluetooth　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/bluetooth">スマホと接続(テザリング)</a></li>\
                            </ul>\
                        </li>\
                <li><a href="javascript:void(0)">RS485(USB変換)　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/minimalmodbus-rs485">MinimalModbus</a></li>\
                            </ul>\
                        </li>\
                <li><a href="javascript:void(0)">Camera　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/camera">写真/動画</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/mjpg-streamer">ビデオストリーム</a></li>\
                            </ul>\
                        </li>\
                <li><a href="https://www.raspberrypirulo.net/entry/watchdog-timer">Watchdog Timer</a>\
                        </li>\
                </ul>\
                </li>\
                <li class="first-content">\
                    <a href="javascript:void(0)"><img class="rasp-icon" src="https://cdn-ak.f.st-hatena.com/images/fotolife/r/raspberrypi/20191122/20191122221737.png" alt="raspberry pi"/>&nbsp;&nbsp;ラズパイ ソフトウェア ▼</a>\
                    <ul class="second-content">\
                        <li><a href="javascript:void(0)">AI　　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/tensorflow">TensolFlow</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/scikit-learn">scikit-learn</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">Socket通信　　　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/socket-server">サーバー編</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/socket-client">クライアント編</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">SocketIO通信 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/socketio-server">サーバー編</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/socketio-client">クライアント編</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/socketio-web">Webページ編</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">データベース　　　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/mariadb-install">MariaDBインストール</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/mysql-workbench">Workbench</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/mysql-python">Pythonプログラム</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">Django　　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/django-1">使い方 その1</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/django-2">使い方 その2</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/django-3">使い方 その3</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">音声　　　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/open-jtalk">音声合成(Open Jtalk)</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/julius">音声認識(julius)</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/python-shellcmd">シェルコマンド実行</a>\
                    　</li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/usb-script">USB挿入でシャットダウン</a>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/apache">Webサーバー(Apache)</a>\
                        </li>\
                    </ul>\
                </li>\
                <li class="first-content">\
                    <a href="javascript:void(0)"><i class="fab fa-python"></i>  Python ▼</a>\
                    <ul class="second-content">\
                        <li><a href="javascript:void(0)">プログラム入門 　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/python-basic">基本構文</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/python-import">importの使い方</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">クラス　　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/python-class-basic">基本</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/python-class-inheritance">継承</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">Thread　　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/thread">基本</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/thread-lock">排他制御</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="javascript:void(0)">MultiProcessing　 ▶</a>\
                            <ul class="third-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/multiprocess">基本</a></li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/multiprocess-sharememory">共有メモリ</a></li>\
                            </ul>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/python-logging">ログ出力(loggingモジュール)</a>\
                    　</li>\
                    </ul>\
                </li>\
                <li class="first-content">\
                    <a href="javascript:void(0)"><i class="fas fa-coffee"></i>  コーヒーブレーク ▼</a>\
                    <ul class="second-content">\
                        <li><a href="http://www.raspberrypirulo.net/archive/category/%E3%83%97%E3%83%AD%E3%82%B0%E3%83%A9%E3%83%9F%E3%83%B3%E3%82%B0%E6%8A%80%E8%A1%93">プログラミング技術</a>\
                        </li>\
                        <li><a href="http://www.raspberrypirulo.net/archive/category/%E3%82%A8%E3%83%B3%E3%82%B8%E3%83%8B%E3%82%A2%E3%81%AE%E6%80%9D%E3%81%84">エンジニアの思い</a>\
                        </li>\
                    </ul>\
                </li>\
            </ul>\
        </div>\
        </div>'
        );
    }
    else{

        $('#title').html('<a href="https://www.raspberrypirulo.net/hatena"><font color="#fff">初心者必見！はてなブログを使いこなす ☆彡</font></a>');
        $('#blog-description').html('<font color="#fff">はてなブログの基本的な使い方、カスタマイズ方法、SEOやアフィリエイトについてわかりやすく解説</font>');

        $('#sw-menu').before(
        '<div id="menu">\
            <div id="menu-inner">\
                <div id="btn-content">\
                    <span id="menu-btn"><i class="blogicon-reorder"></i> MENU</span>\
                </div>\
                <ul id="menu-content">\
                    <li cls="first-content">\
                        <a href="https://www.raspberrypirulo.net/hatena"><i class="blogicon-home lg"></i></a>\
                    </li>\
                    <li class="first-content">\
                        <a href="javascript:void(0)">はてなブログ 基本</a>\
                        <ul class="second-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/hatena-url">URLのカスタマイズ</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/hatena-markdown">Markdown記法</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/hatena-responsible">レスポンシブ、パンくずリスト</a>\
                            </li>\
                        </ul>\
                    </li>	\
                    <li class="first-content">\
                        <a href="javascript:void(0)">デザインカスタマイズ</a>\
                        <ul class="second-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/hatena-photo-center">写真を中央寄せ</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/hatena-htag">hタグをカスタマイズ</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/hatena-update-date">更新日を自動表示</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/hatena-notfound">404 Not Found ページ</a>\
                            </li>\
                        </ul>\
                    </li>\
                    <li class="first-content">\
                        <a href="javascript:void(0)">SEO アフィリエイト</a>\
                        <ul class="second-content">\
                            <li><a href="javascript:void(0)">A8.net</a>\
                            </li>\
                            <li><a href="javascript:void(0)"">もしもアフィリエイト</a>\
                            </li>\
                        </ul>\
                    </li>\
                </ul>\
            </div>\
        </div>'
        );
    }
    
    var menuBtn = $("#menu-btn"),
    menuContent = $("#menu-content");
    menuBtn.click(function(){
        menuContent.slideToggle();
    });
    $(window).resize(function(){
        var win = $(window).width(),
            p = 960;
        if(win > p){
            menuContent.show();
        }else{
            menuContent.hide();
        }
    });    
});
