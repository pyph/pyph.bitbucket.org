[f:id:raspberrypi:20200126175412j:plain:alt=ssh setting]
<span id="share-dup"></span>

<span id="top-google"></span>

<span id="ad-link"></span>

<div class="p-34box">
    <p class="p-34text">この記事の内容はRaspberry Pi 4と3の両方で動作確認済みです</p>
</div>

ブログ管理者のP.Hです！

<span id="ad-banner"></span>

[:contents]

<div class="memo_box">
    <p class="atention">注意事項</p>
    <ul>
        <li>IDとパスワードでのログインは、鍵認証でログインができることを確認後に、無効にしたほうが良いです。鍵認証がうまくいっていないと、SSH接続する方法がなくなってしまいます。</li>
    </ul>
</div>


<div class="code-title" data-title="test.service">
```sh
```
</div>