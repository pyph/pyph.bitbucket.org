$(function() {

    var str = location.href.split('/')
    console.log(str[str.length-1].substring(0, 6));
    // if (((str[str.length-1].substring(0, 6)) != 'hatena') && (str[str.length-1].substring(0, 6)) != '%E3%81'){
    if (((str[str.length-1].substring(0, 5)) != 'point') && (str[str.length-1].substring(0, 5)) != '%E3%81'){

        $('#title > a:nth-child(1)').html('<a href="https://www.raspberrypirulo.net/home" style="color:#343638">Raspberry Pi &amp; Python 開発ブログ ☆彡</a>');
        $('#blog-description').html('<font color="#343638">Raspberry PiとPythonの基本的な使い方をわかりやすく解説。初心者、入門者必見！！</font>');
      
        $('#sw-menu').before(
            '<div id="menu">\
            <div id="menu-inner">\
                <div id="btn-content">\
                    <span id="menu-btn"><i class="blogicon-reorder"></i> MENU</span>\
                </div>\
                <ul id="menu-content">\
                    <li class="first-content">\
                        <a href="https://www.raspberrypirulo.net/home"><i class="fa fa-home"  aria-hidden="true"></i> HOME</a>\
                    </li>\
                    <li class="first-content">\
                        <a href="javascript:void(0)"><img class="rasp-icon" src="https://cdn-ak.f.st-hatena.com/images/fotolife/r/raspberrypi/20191122/20191122221737.png" alt="raspberry pi"/>SETTING <i class="fas fa-angle-down"></i></a>\
                        <ul class="second-content">\
                            <li><a href="javascript:void(0)">セットアップ  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/initial-setup">OSインストール</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">開発環境  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/pipenv">仮想環境(pipenv)</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/vscode">VS Code</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/eclipse-remotesystemexplorer">eclipse</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/teraterm-ssh-macro">TeraTerm SSH自動ログイン</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">自動起動  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/systemd">Systemd</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/cron">Cron</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">ネットワーク  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/wirelesslan">無線LAN設定 DHCP/固定IP</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/ssh">SSH パスワード/鍵認証</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/wifi-trouble">トラブル解決</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">リモート操作  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/remote-desktop">リモートデスクトップ</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/vnc">VNC</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/anydesk">AnyDesk</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">USBデバイス名固定  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/usbname-fix-id">ID指定</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/usbname-fix-port">ポート番号指定</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/samba">Samba設定</a>\
                            </li>\
                        </ul>\
                    </li>\
                    <li class="first-content">\
                        <a href="javascript:void(0)"><img class="rasp-icon" src="https://cdn-ak.f.st-hatena.com/images/fotolife/r/raspberrypi/20191122/20191122221737.png" alt="raspberry pi"/>HARD <i class="fas fa-angle-down"></i></a>\
                        <ul class="second-content">\
                            <li><a href="javascript:void(0)">IO  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/gpio">GPIO(Lチカ)</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/python-callback">割り込み(callback関数)</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/python-waitforedge">割り込み(wait for edge)</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/voltlevel-conv">電圧レベル変換</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">UART  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/uart-setting">シリアル通信設定</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/uart-python">Pythonプログラム</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">I2C  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/raspberrypi-i2c">シリアル通信</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/real-time-clock">リアルタイムクロック</a></li>\
                                </ul>\
                            </li>\
                    <li><a href="javascript:void(0)">USB  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/usb-serial">シリアル通信</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/usbmemory-mount">USBメモリマウント</a></li>\
                                </ul>\
                            </li>\
                    <li><a href="javascript:void(0)">LAN  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/socket-server">Socket通信(サーバー編)</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/socket-client">Socket通信(クライアント編)</a></li>\
                                </ul>\
                            </li>\
                    <li><a href="javascript:void(0)">Audio  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="javascript:void(0)">アナログ出力</a></li>\
                                <li><a href="javascript:void(0)">USBスピーカー</a></li>\
                                </ul>\
                            </li>\
                    <li><a href="javascript:void(0)">Bluetooth  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/bluetooth">スマホと接続(テザリング)</a></li>\
                                </ul>\
                            </li>\
                    <li><a href="javascript:void(0)">RS485(USB変換)  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/minimalmodbus-rs485">MinimalModbus</a></li>\
                                </ul>\
                            </li>\
                    <li><a href="javascript:void(0)">Camera  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/camera">写真/動画</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/mjpg-streamer">ビデオストリーム</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/opencv">OpenCVで顔認識</a></li>\
                                </ul>\
                            </li>\
                    <li><a href="https://www.raspberrypirulo.net/entry/watchdog-timer">Watchdog Timer</a>\
                            </li>\
                    </ul>\
                    </li>\
                    <li class="first-content">\
                        <a href="javascript:void(0)"><img class="rasp-icon" src="https://cdn-ak.f.st-hatena.com/images/fotolife/r/raspberrypi/20191122/20191122221737.png" alt="raspberry pi"/>SOFT <i class="fas fa-angle-down"></i></a>\
                        <ul class="second-content">\
                            <li><a href="javascript:void(0)">データベース  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/mariadb-install">MariaDBインストール</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/mysql-workbench">Workbench</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/mysql-python">Pythonプログラム</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">音声  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/open-jtalk">音声合成(Open Jtalk)</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/julius">音声認識(julius)</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/julius-python">julius+python連携</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/python-shellcmd">シェルコマンド実行</a>\
                        　</li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/usb-script"><span style="font-size:11px">USB挿入でシャットダウン</span></a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/apache">Webサーバー(Apache)</a>\
                            </li>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/scratch3">Scratch3の使い方</a>\
                            </li>\
                        </ul>\
                    </li>\
                    <li class="first-content">\
                        <a href="javascript:void(0)"><i class="fab fa-python"></i>  Python <i class="fas fa-angle-down"></i></a>\
                        <ul class="second-content">\
                            <li><a href="javascript:void(0)">プログラム入門  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/python-basic">基本構文</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/python-import">importの使い方</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">クラス  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/python-class-basic">基本</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/python-class-inheritance">継承</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">Thread  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/thread">基本</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/thread-lock">排他制御</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">MultiProcessing  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/multiprocess">基本</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/multiprocess-sharememory">共有メモリ</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">WebSocket通信  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/websocket-server">サーバー編</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/websocket-client">クライアント編</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/websocket-web">Webページ編</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">SocketIO通信  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/socketio-server">サーバー編</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/socketio-client">クライアント編</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/socketio-web">Webページ編</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">Django  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/django-1">使い方 その1</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/django-2">使い方 その2</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/django-3">使い方 その3</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="javascript:void(0)">AI  <i class="fas fa-angle-right"></i></a>\
                                <ul class="third-content">\
                                <li><a href="https://www.raspberrypirulo.net/entry/tensorflow">TensolFlow v1</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/tensorflow2">TensolFlow v2</a></li>\
                                <li><a href="https://www.raspberrypirulo.net/entry/scikit-learn">scikit-learn</a></li>\
                                </ul>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/python-logging">ログ出力(logging)</a>\
                          　</li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/selenium"><span style="font-size:11px">スクレイピング(selenium)</span></a>\
                          　</li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/excel-python"><span style="font-size:11px">Excel操作</span></a>\
                          　</li>\
                        </ul>\
                    </li>\
                    <li class="first-content">\
                        <a href="javascript:void(0)"><i class="fas fa-coffee"></i> プログラム学習 <i class="fas fa-angle-down"></i></a>\
                        <ul class="second-content">\
                            <li><a href="https://www.raspberrypirulo.net/entry/beginner-road">初心者脱出</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/memory-address">メモリアドレス</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/python-doc">ドキュメント</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/object-overview">オブジェクト指向概要</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/object-class">クラス基本</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/encapsulation">カプセル化</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/inheritance">継承</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/polymorphism">多様性</a>\
                            </li>\
                            <li><a href="https://www.raspberrypirulo.net/entry/class-example">クラス実例</a>\
                            </li>\
                            <li><a href="http://www.raspberrypirulo.net/archive/category/%E3%82%A8%E3%83%B3%E3%82%B8%E3%83%8B%E3%82%A2%E3%81%AE%E6%80%9D%E3%81%84">エンジニアの思い</a>\
                            </li>\
                        </ul>\
                    </li>\
                </ul>\
            </div>\
            </div>'
        );
    }
    else{

        $('#title > a:nth-child(1)').html('<a href="https://www.raspberrypirulo.net/point" style="color:#343638">ポイ活のすゝめ！超簡単副業！！</a>');
        $('#blog-description').html('<font color="#343638">ポイ活でプチ贅沢！最速でポイントをゲットする方法を詳しく解説！！</font>');

        $('#sw-menu').before(
        '<div id="menu">\
        <div id="menu-inner">\
            <div id="btn-content">\
                <span id="menu-btn"><i class="blogicon-reorder"></i> MENU</span>\
            </div>\
            <ul id="menu-content">\
                <li cls="first-content">\
                    <a href="https://www.raspberrypirulo.net/point"><i class="fa fa-home"  aria-hidden="true"></i> HOME</a>\
                </li>\
                <li class="first-content">\
                    <a href="javascript:void(0)"><i class="fas fa-pen" aria-hidden="true"></i> ポイ活の基本</a>\
                    <ul class="second-content">\
                        <li><a href="https://www.raspberrypirulo.net/entry/point-moppy">モッピーとは</a>\
                        </li>\
                    </ul>\
                </li>\
                <li class="first-content">\
                    <a href="javascript:void(0)"><i class="fas fa-palette" aria-hidden="true"></i> ゲームアプリ案件</a>\
                    <ul class="second-content">\
                        <li><a href="https://www.raspberrypirulo.net/entry/point-rts">RTSの解説</a>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/point-evony">エボニー 4スター</a>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/point-lordsmobile">ロードモバイル 城15</a>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/point-mafia">マフィアシティ 美女4人</a>\
                        </li>\
                        <li><a href="javascript:void(0)">エイジオブゼット</a>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/point-higawari">日替わり内室 150万</a>\
                        </li>\
                    </ul>\
                </li>\
                <li class="first-content">\
                    <a href="javascript:void(0)"><i class="far fa-thumbs-up" aria-hidden="true"></i> その他案件</a>\
                    <ul class="second-content">\
                        <li><a href="https://www.raspberrypirulo.net/entry/point-unext">U-NEXT初回登録</a>\
                        </li>\
                        <li><a href="javascript:void(0)">U-NEXTシネマ見放題</a>\
                        </li>\
                    </ul>\
                </li>\
            </ul>\
        </div>\
        </div>'
        );


        /*        
        $('#title > a:nth-child(1)').html('<a href="https://www.raspberrypirulo.net/hatena" style="color:#343638">初心者必見！はてなブログを使いこなす！！</a>');
        $('#blog-description').html('<font color="#343638">はてなブログの基本的な使い方、カスタマイズ方法、SEOやアフィリエイトについてわかりやすく解説</font>');

        $('#sw-menu').before(
        '<div id="menu">\
        <div id="menu-inner">\
            <div id="btn-content">\
                <span id="menu-btn"><i class="blogicon-reorder"></i> MENU</span>\
            </div>\
            <ul id="menu-content">\
                <li cls="first-content">\
                    <a href="https://www.raspberrypirulo.net/hatena"><i class="fa fa-home"  aria-hidden="true"></i> HOME</a>\
                </li>\
                <li class="first-content">\
                    <a href="javascript:void(0)"><i class="fas fa-pen" aria-hidden="true"></i> はてなブログ 基本</a>\
                    <ul class="second-content">\
                        <li><a href="https://www.raspberrypirulo.net/entry/hatena-url">URLのカスタマイズ</a>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/hatena-markdown">Markdown記法</a>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/hatena-responsible"><span style="font-size:11px">レスポンシブ、パンくずリスト</span></a>\
                        </li>\
                    </ul>\
                </li>\
                <li class="first-content">\
                    <a href="javascript:void(0)"><i class="fas fa-palette" aria-hidden="true"></i> デザインカスタマイズ</a>\
                    <ul class="second-content">\
                        <li><a href="https://www.raspberrypirulo.net/entry/hatena-photo-center">写真を中央寄せ</a>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/hatena-htag">hタグをカスタマイズ</a>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/hatena-update-date">更新日を自動表示</a>\
                        </li>\
                        <li><a href="https://www.raspberrypirulo.net/entry/hatena-notfound">404 Not Found ページ</a>\
                        </li>\
                    </ul>\
                </li>\
                <li class="first-content">\
                    <a href="javascript:void(0)"><i class="far fa-thumbs-up" aria-hidden="true"></i> SEO アフィリエイト</a>\
                    <ul class="second-content">\
                        <li><a href="javascript:void(0)">A8.net</a>\
                        </li>\
                        <li><a href="javascript:void(0)">もしもアフィリエイト</a>\
                        </li>\
                    </ul>\
                </li>\
            </ul>\
        </div>\
        </div>'
        );
        */
    }
    
    var menuBtn = $("#menu-btn"),
    menuContent = $("#menu-content");
    menuBtn.click(function(){
        menuContent.slideToggle();
    });
    $(window).resize(function(){
        var win = $(window).width(),
            p = 960;
        if(win > p){
            menuContent.show();
        }else{
            menuContent.hide();
        }
    });    
});
