$(document).ready(function() {
  if ($('#main-inner').children().hasClass("no-entry")) {
  if ($('.entry-content > h1:nth-child(1)').text() == 'Not Found') {
    
    $('#main-inner').html("<div class=\"notfound\">\
    <br/>ページが見つからず、大変申し訳ございません。<br/>\
    最近、大幅にURLの変更を致しました。また、記事の内容も最新のものにアップデートしています。<br/>\
    お手数ですが、下記のトップページから記事を探して頂き、ご覧下さい。<br/>\
    <br/><br/>\
    <div class=\"link-box\">\
      <a class=\"link-box-a\" href=\"https://www.raspberrypirulo.net/home\">\
        <div class=\"link-box-label\">サイトマップ</div>\
        <span class=\"link-tbcell link-tbimg\">\
            <img src=\"https://cdn-ak2.f.st-hatena.com/images/fotolife/r/raspberrypi/20200119/20200119103848.png\">\
        </span>\
        <div class=\"link-tbcell link-tbtext\">\
          <span>Python &amp; Raspberry Pi 開発ブログ ☆彡</span><br>\
          <span style=\"color:#000; font-size:14px; font-weight:400;\">PythonとRaspberry Piの基本的な使い方をわかりやすく解説。初心者、入門者必見！！</span>\
        </div>\
      </a>\
  </div>\
  <!---\
  <div class=\"link-box\">\
      <a class=\"link-box-a\" href=\"https://www.raspberrypirulo.net/hatena\">\
        <div class=\"link-box-label\">サイトマップ</div>\
        <span class=\"link-tbcell link-tbimg\">\
            <img src=\"https://cdn-ak.f.st-hatena.com/images/fotolife/r/raspberrypi/20200119/20200119100517_120.jpg\">\
        </span>\
        <div class=\"link-tbcell link-tbtext\">\
          <span>初心者必見！はてなブログを使いこなす！！</span><br>\
          <span style=\"color:#000; font-size:14px; font-weight:400;\">はてなブログの基本的な使い方、カスタマイズ方法についてわかりやすく解説</span>\
        </div>\
      </a>\
  </div>\
  --->\
  ");
  $('.notfound').css('margin','auto 20px');
  }
  }
});