[f:id:raspberrypi:20200201121636j:plain:alt=raspberry pi all]

<span id="share-dup"></span>

<span id="top-google"></span>

<span id="ad-link"></span>

ブログ管理者のP.Hです！

今回はRaspbery Piユーザーに向けに、使い方を紹介した記事をまとめました。こんな使い方があったのかと気づくことがあるかもしれませんので、ぜひ一度ご覧ください。

<span id="ad-banner"></span>

[:contents]


## OSインストール
OSインストール手順を解説しているサイトです。Raspbery Piを購入したら、まず下記の記事を参考にOSのインストールを行いましょう。

* Raspbian(OS)のインストール  
<a class="all-link" href="https://www.raspberrypirulo.net/entry/initial-setup">Raspberry Pi：OSインストールと初期セットアップ【保存版】</a>

* NOOBS(OSをインストールするためのソフト)のインストール
<a class="all-link" href="https://raspida.com/raspixnoobs">Raspberry PiにNOOBSでOSをインストールする方法</a>

## 無線LAN(Wi-Fi)接続
OSインストール後に行うのは無線LANの設定ですね。無線LANで接続 ⇒ SSH接続、という流れでネットワーク経由で操作される方が多いと思います。SSH接続ができれば、
HDMIモニタ、キーボード、マウスがなくても操作することができます。

* 無線LANの接続方法
<a class="all-link" href="https://www.raspberrypirulo.net/entry/wirelesslan">Raspbery Piで無線LAN接続する。DHCP/固定IP設定の解説付き！</a>

* SSH接続の設定方法
<a class="all-link" href="https://www.raspberrypirulo.net/entry/ssh">Raspbery PiにSSH接続する方法 パスワード/鍵認証の両方解説！</a>

* SSIDがステルス設定の場合
<a class="all-link" href="https://itengine.seesaa.net/article/458955047.html">Raspberry Pi 3をUpdateしたらWiFiに繋がらなくなった: 文系エンジニアの私的ナレッジベース</a>


* 無線LAN接続できない時の対処法
<a class="all-link" href="https://www.raspberrypirulo.net/entry/wifi-trouble">Raspberry Piで無線LANやインターネットに繋がらない時の対処方法</a>

## 開発環境
何でもそうですが、環境はとても大事だと思います。良い環境で開発できれば、作業効率があがったり品質の向上に繋がります。

* Pipenv(pythonのバージョンとインストールしたモジュールの管理)の使い方
<a class="all-link" href="https://www.raspberrypirulo.net/entry/pipenv">Pipenvで仮想環境を作成する</a>

* VS CODEの導入手順を紹介。最近の開発では、VS CODE一択？というくらい使いやすいです。
<a class="all-link" href="https://www.raspberrypirulo.net/entry/vscode">VS Code以外何もいらない！超軽快にSSH接続してpythonコード書いて実行してデバックまでできる！！</a>

* VS CODEの拡張機能の紹介サイト。
<a class="all-link" href="https://www.hypertextcandy.com/visual-studio-code-extensions">Visual Studio Code拡張機能 14選 | Hypertext Candy</a>

## リモート接続
リモートでデスクトップ画面を操作する方法の紹介です。AnyDeskというソフトはインターネットに繋がる環境であれば、どこでもリモート操作可能です。

* VNC これを使っている方が一番多いのではないでしょうか。
<a class="all-link" href="https://www.raspberrypirulo.net/entry/vnc">Raspberry PiにVNCで接続して、デスクトップをリモート操作する</a>

* リモートデスクトップ　Windowsであれば、ソフトウェアのインストール不要でリモート操作できます。
<a class="all-link" href="https://www.raspberrypirulo.net/entry/remote-desktop">Raspberry Piにリモートデスクトップで接続して、リモート操作する</a>


* AnyDesk 無料、ネット環境があればどこでもリモート接続可能
<a class="all-link" href="https://www.raspberrypirulo.net/entry/anydesk
">AnyDeskを使用してRaspberry Piをリモート操作する</a>


## プログラムの自動起動
タイムスケジュールや起動時にサービスとしてプログラムを実行する手順の紹介です。

* Cron タイムスケジュールでプログラム実行する機能
<a class="all-link" href="https://www.raspberrypirulo.net/entry/cron">Raspberry Piでcronを使ってPythonを定期実行する</a>

* Systemd サービス(常駐プログラム)の設定、実行
<a class="all-link" href="https://www.raspberrypirulo.net/entry/systemd">Raspberry Piでsystemdを使ってpythonを自動実行する</a>

## Scratchの使い方
プログラミングが学校教育に取り入れられる時代になりましたね。もともとRaspbery Piは教育用途でされたものなので、最適ですね。話題のScratchを使って勉強しましょう。

* Scratch 3の使い方
<a class="all-link" href="https://www.raspberrypirulo.net/entry/scratch3">Raspberry Pi + Scratch 3でLEDを光らせる</a>

## GPIO(汎用入出力)の使い方
Raspbery Piの40pinの使い方です。LEDを光らせたり、スイッチのON/OFF状態を監視したりすることができます。GPIOを使うと、モーターを動かしたりセンサーの値を取得できたり、いろいろなことができるようになります。

* GPIOの基本的な使い方
<a class="all-link" href="https://www.raspberrypirulo.net/entry/gpio
">Raspberry PiでLEDを光らせてGPIOの使い方をマスターする！</a>

* 小型Servo SG90の使い方
<a class="all-link" href="https://shizenkarasuzon.hatenablog.com/entry/2019/03/04/002150
">【Raspberry pi】サーボモータをPWM制御する - とある科学の備忘録</a>

* 温度センサー DHT11の使い方
<a class="all-link" href="https://www.souichi.club/raspberrypi/temperature-and-humidity/
">ラズパイで温湿度を測定（DHT11） | そう備忘録</a>

## UARTの使い方
UARTポート使用することで、シリアル接続のデバイスを制御することができます。その時の設定の紹介です。また、ネット環境がない場合は、シリアルコンソールと使うことができ、Raspbery Piを操作することができます。

* UARTの設定方法：コンソールと汎用通信
<a class="all-link" href="https://www.raspberrypirulo.net/entry/uart-setting
">Raspberry PiでUART(シリアル)通信：コンソールと汎用通信の設定</a>

* UARTの使い方：コンソールとpythonで汎用通信
<a class="all-link" href="https://www.raspberrypirulo.net/entry/uart-python
">Raspberry PiでUART(シリアル)通信：Pythonプログラムとコンソールの使い方</a>

## I2Cの使い方
I2Cは同じ通信ラインで複数台制御できるのが特徴です。また、RTC(リアルタイムクロック)や小型LCDはを使う時に、I2Cを使うことが多いです。

* I2C通信の基本
<a class="all-link" href="https://www.raspberrypirulo.net/entry/raspberrypi-i2c
">I2Cの使い方</a>

* RTCの使い方
<a class="all-link" href="https://www.raspberrypirulo.net/entry/real-time-clock
">リアルタイムクロック(RTC)の使い方</a>

## USBの使い方
USBでシリアル通信をして、Ardinoと連携したり、シリアル接続のデバイスを制御できます。また、RS485変換器を使うことで、Modbus通信も可能になります。その他にもUSBメモリを使ったり、USBデバイス名の固定することができます。

* USBでシリアル通信して、Arduinoと連携する
<a class="all-link" href="https://www.raspberrypirulo.net/entry/usb-serial
">Raspberry piとArduino連携：USBでシリアル通信をする</a>

* RS485接続をして、Modbus通信をする
<a class="all-link" href="https://www.raspberrypirulo.net/entry/minimalmodbus-rs485
">MinimalModbusモジュールでRS485通信をする</a>

* USBメモリを使用する
<a class="all-link" href="https://www.raspberrypirulo.net/entry/usbmemory-mount
">USBメモリをマウントする</a>

* USBデバイス名を固定する
<a class="all-link" href="https://www.raspberrypirulo.net/entry/usbname-fix-id
">Raspberry PiでUSBのデバイス名を固定する：ID指定</a>
<a class="all-link" href="https://www.raspberrypirulo.net/entry/usbname-fix-port
">Raspberry PiでUSBのデバイス名を固定する：USBポート番号指定</a>

## LANの使い方
最近はLANで接続して、Socket通信で制御することが本当に増えました。その時の設定、使い方です。

* Sokect通信の仕方
<a class="all-link" href="https://www.raspberrypirulo.net/entry/socket-server
">Python Socket通信の仕方：サーバー側</a>
<a class="all-link" href="https://www.raspberrypirulo.net/entry/socket-client
">Python Socket通信の仕方：クライアント編</a>

## Audioの使い方
Audioの使い方の紹介です。Volumio、めちゃくちゃかっこいいですね。

* イヤホンジャックで音声出力
<a class="all-link" href="https://tomosoft.jp/design/?p=33680
">100均スピーカをRaspberry Pi 3の3.5mmジャックに接続 | TomoSoft</a>

* Volumioの使い方
<a class="all-link" href="https://inakadeikinaosu.com/shumi/raspi20180615/
">Raspberry PiとVolumioでハイレゾオーディオを作る。 | 田舎で生きナオス</a>


## Bluetoothの使い方
Bluetoothの接続の方法の紹介です。スマホとテザリングしたり、スピーカーを接続することもできます。

* Bluetoothでスマホと接続(テザリング)
<a class="all-link" href="https://www.raspberrypirulo.net/entry/bluetooth
">Bluetoothでスマホと接続して、テザリングする</a>

* Bluetoothスピーカーの使い方
<a class="all-link" href="https://sakayasu.net/raspberrypi-speaker/
">【ラズベリーパイ（ラズパイ）】ラズパイをスピーカーにもっとも簡単に接続する方法！ | さかやすプログラミング教室・サークル</a>

## Cameraモジュールの使い方
Cameraモジュールで写真や動画撮影ができます。また、ストリーミングの機能を使えば、遠隔監視もできてしまいます。また、USBカメラももちらん使用できます。

* 写真/動画撮影する方法
<a class="all-link" href="https://www.raspberrypirulo.net/entry/camera
">Raspberry Piにカメラを接続して、写真/動画を撮影する！</a>

* ストリーミング映像を見る方法
<a class="all-link" href="https://www.raspberrypirulo.net/entry/mjpg-streamer
">Raspberry Piでストリーミング映像を配信する。Mjpg-Streamerで超簡単！</a>

* USBカメラ motionの使い方がわかりやすく説明されています。
<a class="all-link" href="https://rikei-life.com/2019/03/16/raspberry-pi-motion/
">Raspberry Pi（ラズパイ）とWebカメラを使って動体検知してみた（motion）｜りけろぐ</a>

## 音声合成、音声認識
Raspberry Piで言葉をしゃべったり、逆に言葉を認識して文字として出力することができます。

* OpenJtalkで音声合成する
<a class="all-link" href="https://www.raspberrypirulo.net/entry/open-jtalk
">Open JTalkで音声合成する(しゃべるラズパイ)</a>

* AquesTalk Piで音声合成する 本家のページです。簡単すぎます。。
<a class="all-link" href="https://www.a-quest.com/products/aquestalkpi.html
">https://www.a-quest.com/products/aquestalkpi.html</a>

* Juliusで音声認識する
<a class="all-link" href="https://www.raspberrypirulo.net/entry/julius
">juliusで音声認識をする</a>

## NASサーバー
ネットワーク上でストレージを共有するサーバーです。データをサーバー上に置いておけば、PCでもスマホでもデータを使えるようになります。

* NASサーバー ベンチマーク結果まで詳細がわかります
<a class="all-link" href="https://misoji-engineer.com/archives/raspberrypi-sever.html
">ラズパイのサーバーを性能比較！NASの速度・負荷を確認してみた</a>

## Webサーバー
Webサーバーを起動すれば、Webページを表示することができます。

* Apache
<a class="all-link" href="https://www.raspberrypirulo.net/entry/apache
">Webサーバーを構築する：Apache+PHP</a>

* nginx
<a class="all-link" href="https://pcmanabu.com/raspberry-pi4-webserver/
">Raspberry Pi 4 Webサーバーを構築する PCまなぶ</a>

## Mediaサーバー
DLNAサーバーです。PC等のDLNA対応機器であれば、動画や音楽を再生することができるようになります。

* Raspberry Pi Zero で　DLNAサーバー(MiniDLNA)
<a class="all-link" href="https://memotoreboot.o-oi.net/raspberrypi_minidlna/180102
">Raspberry Pi Zero で DLNAサーバー(MiniDLNA)｜Raspberrypi-miniDLNA｜おっさん。メモっと！ リブート</a>



本当にRaspbery Piはいろいろなことができます。また、Raspbery Pi 4はノートパソコンとして、普通に使えるレベルまでスペックが向上しています。これが1万円以下で簡単に入手できるとは、本当に便利な時代になりましたね。ぜひ、使ってみてください！


